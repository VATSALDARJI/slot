import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View,TextInput, TouchableOpacity, Dimensions, Button } from 'react-native'
import { connect } from 'react-redux'

// import { TextField } from '../Components/material-textfield';
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import SlotActions from '../Redux/SlotsRedux'

// Styles
import styles from './Styles/DetailsScreenStyle'

const {width} = Dimensions.get('window')

class DetailsScreen extends Component {
  state={
    name:'',
    phoneNumber:'',
  }
  componentDidMount(){
    console.log('selected slot : ',this.props.selectedSlotId)
    const {slots,selectedSlotId} = this.props
    if(slots[selectedSlotId].isBooked){
      this.setState({name:slots[selectedSlotId].name,phoneNumber:slots[selectedSlotId].mobile})
    }
  }
  onSavePress = () => {
    this.props.dispatch(SlotActions.editSlot({
      id:this.props.selectedSlotId,
      name:this.state.name,
      mobile:this.state.phoneNumber
    }))
    this.props.navigation.goBack()
    // setTimeout(() => {
    //   console.log('new data : ',this.props.slots)
    // },500)
  }
  render () {
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          {/* <Text>DetailsScreen</Text> */}
          <View style={{width:'100%'}}>
          <TextInput
            placeholder={'name'}
            value={this.state.name}
            onChangeText={text => this.setState({name:text})}
            style={{width:width - (width/8)}}
            underlineColorAndroid={'red'}
          />
          <TextInput
            placeholder={'phone number'}
            keyboardType='phone-pad'
            value={this.state.phoneNumber}
            onChangeText={text => this.setState({phoneNumber:text})}
            style={{width:width - (width/8)}}
            underlineColorAndroid={'red'}
          />
          <View style={{flexDirection:'row',width:'100%',}}>
            <View style={{flex:1,paddingRight:10}}>
              <Button style={{flex:1}} title='Save' onPress={this.onSavePress}/>
            </View>
            <View style={{flex:1,paddingRight:10}}>
            <Button style={{flex:1}} title='Cancle' onPress={() => this.props.navigation.goBack()}/>
            </View>
          </View>
          {/* <TouchableOpacity onPress={this.onSavePress}>
            <Text>Save</Text>
          </TouchableOpacity> */}
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    selectedSlotId: state.slot.selectedSlotId,
    slots: state.slot.data,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen)
