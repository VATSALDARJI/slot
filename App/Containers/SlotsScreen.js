import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, FlatList, View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import SlotActions from '../Redux/SlotsRedux'
// Styles
import styles from './Styles/SlotsScreenStyle'

class SlotsScreen extends Component {
  onSlotPress = (id) => {
    this.props.dispatch(SlotActions.selectSlot(id))
    this.props.navigation.navigate('DetailsScreen')
  }
  _renderItem = ({item,index}) => {
    console.log('item startdate : ',item.startTime.getHours())
    return(
      <TouchableOpacity style={{justifyContent:'center',paddingHorizontal:10,paddingVertical:30}} onPress={() => this.onSlotPress(item.id)}>
        <Text>{item.startTime.getHours()}:{item.startTime.getMinutes()} - {item.endTime.getHours()}:{item.endTime.getMinutes()}</Text>
        <View style={{flexDirection:'row',marginTop:5}}>
          <View style={{height:20,width:20,borderRadius:10,backgroundColor:item.isBooked?'red':'green'}}/>
          <Text numberOfLines={1}>{item.isBooked ? 'Booked by '+item.name : 'Available'}</Text>
        </View>
      </TouchableOpacity>
    )
  }
  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          {/* <Text>SlotsScreen</Text> */}
          <FlatList 
            data={Object.values(this.props.slots)}
            renderItem={this._renderItem}
          />
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    slots: state.slot.data
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SlotsScreen)
