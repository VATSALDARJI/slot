import { createAppContainer } from 'react-navigation'
import DetailsScreen from '../Containers/DetailsScreen'
import SlotsScreen from '../Containers/SlotsScreen'
import { createStackNavigator } from 'react-navigation-stack';
import LaunchScreen from '../Containers/LaunchScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  SlotsScreen: { screen: SlotsScreen ,navigationOptions:{
    title:'Slots',
    // headerTitle:'Slots'
  }},
  DetailsScreen: { screen: DetailsScreen ,navigationOptions:{
    title:'Details',
    headerTitle:'Details'
  }},
  LaunchScreen: { screen: LaunchScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'SlotsScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
