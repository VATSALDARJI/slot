import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  slotsRequest: ['data'],
  slotsSuccess: ['payload'],
  slotsFailure: null,
  selectSlot: ['id'],
  editSlot: ['details'],
})

export const SlotsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  // data: null,
  data: {
    '1' : {
      id:'1',
      isBooked:false,
      name:'',
      mobile:'',
      startTime: new Date(2020,7,15,9,0,0,0),
      endTime: new Date(2020,7,15,17,0,0,0),
    },
    '2' : {
      id:'2',
      isBooked:false,
      name:'',
      mobile:'',
      startTime: new Date(2020,7,15,9,0,0,0),
      endTime: new Date(2020,7,15,17,0,0,0),
    },
    '3' : {
      id:'3',
      isBooked:false,
      name:'',
      mobile:'',
      startTime: new Date(2020,7,15,9,0,0,0),
      endTime: new Date(2020,7,15,17,0,0,0),
    },
    '4' : {
      id:'4',
      isBooked:false,
      name:'',
      mobile:'',
      startTime: new Date(2020,7,15,9,0,0,0),
      endTime: new Date(2020,7,15,17,0,0,0),
    },
    '5' : {
      id:'5',
      isBooked:false,
      name:'',
      mobile:'',
      startTime: new Date(2020,7,15,9,0,0,0),
      endTime: new Date(2020,7,15,17,0,0,0),
    },
    '6' : {
      id:'6',
      isBooked:false,
      name:'',
      mobile:'',
      startTime: new Date(2020,7,15,9,0,0,0),
      endTime: new Date(2020,7,15,17,0,0,0),
    },
  },
  selectedSlotId: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const SlotsSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

export const editSlot = (state,action) => {
  const {details} = action
  console.log('details : ',action)
  return state.merge({data:{...state.data,[details.id]:{...state.data[details.id],name:details.name,mobile:details.mobile,isBooked:true}}})
}

export const selectSlot = (state,action) => {
  console.log('state : ',state)
  const { id } = action
  return state.merge({selectedSlotId:id})
}

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SLOTS_REQUEST]: request,
  [Types.SLOTS_SUCCESS]: success,
  [Types.SLOTS_FAILURE]: failure,
  [Types.SELECT_SLOT]: selectSlot,
  [Types.EDIT_SLOT]: editSlot,
})
